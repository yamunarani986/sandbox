package com.sandbox.model;



import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import com.sandbox.repository.UserRepository;


 
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class UserTests {

	 
	@Autowired
	private UserRepository repo;
	
	@Mock
	UserDtls user;

     
    @Test
    public void testCreateUser() {
		 UserDtls user = new UserDtls();
		 	user.setId((long) 90);
            user.setFirstname("yamuna");
            user.setLastname("rani");
            user.setEmail("yamuna@gmail.com");
            user.setAddress("Hyd");
            user.setNumber("1234567890");
            user.setPassword("yamuna12");
            user.setRole("admin");
            assertNotNull(repo.save(user));
        
    }
    
    @Test
	public void testFinduser() {
		 UserDtls user = new UserDtls();
		 user.setId((long) 9);
		 assertNotNull(user.getId());
	}
    
    @Test
	public void testFinduserfirst() {
		 UserDtls user = new UserDtls();
		 user.setFirstname("yamuna");
		 assertNotNull(user.getFirstname());
	}
    
    @Test
	public void testFinduserlast() {
		 UserDtls user = new UserDtls();
		 user.setLastname("rani");
		 assertNotNull(user.getLastname());
	}
    
    @Test
	public void testFinduseremail() {
		 UserDtls user = new UserDtls();
		 user.setEmail("yamuna@gmail.com");
		 assertNotNull(user.getEmail());
	}
    
    @Test
	public void testFinduseraddress() {
		 UserDtls user = new UserDtls();
		 user.setAddress("Hyd");
		 assertNotNull(user.getAddress());
	}
    
    @Test
	public void testFindusernumber() {
		 UserDtls user = new UserDtls();
		 user.setNumber("1234567890");
		 assertNotNull(user.getNumber());
	}
    
    @Test
	public void testFinduserpassword() {
		 UserDtls user = new UserDtls();
		 user.setPassword("yamuna");
		 assertNotNull(user.getPassword());
	}
    
    @Test
	public void testFinduserrole() {
		 UserDtls user = new UserDtls();
		 user.setRole("Admin");
		 assertNotNull(user.getRole());
	}
}

