package com.sandbox.model;


import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import com.sandbox.repository.CategoryRepository;
import com.sandbox.service.CategoryService;



 
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class CategoryTests {

	 
	@Autowired
	private CategoryRepository repo;
	
	@Mock
	Category category;

	@InjectMocks
	private CategoryService service;
     
    @Test
    public void testCreatecategory() {
		 Category category = new Category();
		 	category.setId((long) 90);
            category.setName("yamuna");
           
            assertNotNull(repo.save(category));
        
    }
    
    @Test
	public void testFindcategory() {
    	Category category = new Category();
		 category.setId((long) 9);
		 assertNotNull(category.getId());
	}
    
    @Test
	public void testFindname() {
    	Category category = new Category();
		 category.setName("mobiles");
		 assertNotNull(category.getName());
	}
    
    
    
}
    
