package com.sandbox.model;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import com.sandbox.repository.ProductRepository;



@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class ProductTests {

	 
	@Autowired
	private ProductRepository repo;
	
	@Mock
	Product product;
	
	@Test
	public void testCreateProduct() {
		 Product product = new Product();
		 	product.setPid((long) 90);
		    product.setName("Samsung zflip");
		    product.setCategory("Mobiles");
		    product.setDescription("best Software");
            product.setPrice(200);
	        product.setStock(4);
	        assertNotNull(repo.save(product));
	         
	       
	         
	    }
	

	@Test
	public void testFindproduct() {
		 Product product = new Product();
		 product.setName("Samsung zflip");
		 assertNotNull(product.getName());
	}
	         
	@Test
	public void testFindproductcategory() {
		 Product product = new Product();
		 product.setCategory("Mobiles");
		 assertNotNull(product.getCategory());
	}
	
	@Test
	public void testFindproductdescription() {
		 Product product = new Product();
		 product.setDescription("good");
		 assertNotNull(product.getDescription());
	}
	
	@Test
	public void testFindproductprice() {
		 Product product = new Product();
		 product.setPrice(200);
		 assertNotNull(product.getPrice());
	}
	
	@Test
	public void testFindproductstock() {
		 Product product = new Product();
		 product.setStock(1);
		 assertNotNull(product.getStock());
	}
	}

