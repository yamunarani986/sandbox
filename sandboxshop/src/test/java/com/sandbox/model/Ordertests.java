package com.sandbox.model;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import com.sandbox.repository.OrderRepository;



@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class Ordertests {

	 
	@Autowired
	private OrderRepository repo;
	
	@Mock
	Orders order;
	
	 @Test
	    public void testCreatecart() {
		 Orders order = new Orders();
		 order.setId(90);
		 order.setName("yamuna");
		 order.setDescription("good");
		 order.setCategory("mobiles");
		 order.setPrice(200);
		 order.setQuantity(2);
	           
	            assertNotNull(repo.save(order));
	        
	    }
	 
	 @Test
		public void testFindid() {
		 Orders order = new Orders();
		 order.setId(1);
			 assertNotNull(order.getId());
		}
	 
	 @Test
		public void testFindname() {
		 Orders order = new Orders();
		 order.setName("samsung");
			 assertNotNull(order.getName());
		}
	 
	 
	 @Test
		public void testFindcate() {
		 Orders order = new Orders();
		 order.setCategory("pooja");
			 assertNotNull(order.getCategory());
		}
	 
	 
	 @Test
		public void testFinddes() {
		 Orders order = new Orders();
		 order.setDescription("best");
			 assertNotNull(order.getDescription());
		}
	 
	 
	 @Test
		public void testFindprice() {
		 Orders order = new Orders();
		 order.setPrice(100);
			 assertNotNull(order.getPrice());
		}
	 
	 
	 @Test
		public void testFindquan() {
		 Orders order = new Orders();
		 order.setQuantity(1);
			 assertNotNull(order.getQuantity());
		}
	 
	 
	 
	 @Test
	 public void Constructor() {
	     Integer id = 1;
	     String name = "sony";
	     String description = "good";
	     String category = "headset";
	     float price = 200;
	     Integer quantity = 1;
	     Orders order = new Orders(id, name, description, category, price, quantity);

	 }
}
