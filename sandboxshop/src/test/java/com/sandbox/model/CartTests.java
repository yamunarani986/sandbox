package com.sandbox.model;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import com.sandbox.repository.CartRepository;



@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class CartTests {

	 
	@Autowired
	private CartRepository repo;
	
	@Mock
	Cart cart;
	
	 @Test
	    public void testCreatecart() {
			 Cart cart = new Cart();
			 	cart.setId(90);
	            cart.setName("yamuna");
	            cart.setDescription("good");
	            cart.setCategory("mobiles");
	            cart.setPrice(200);
	            cart.setQuantity(2);
	           
	            assertNotNull(repo.save(cart));
	        
	    }
	 
	 @Test
		public void testFindid() {
	    	Cart cart = new Cart();
			 cart.setId(1);
			 assertNotNull(cart.getId());
		}
	 
	 @Test
		public void testFindname() {
	    	Cart cart = new Cart();
			 cart.setName("samsung");
			 assertNotNull(cart.getName());
		}
	 
	 
	 @Test
		public void testFindcate() {
	    	Cart cart = new Cart();
			 cart.setCategory("pooja");
			 assertNotNull(cart.getCategory());
		}
	 
	 
	 @Test
		public void testFinddes() {
	    	Cart cart = new Cart();
			 cart.setDescription("best");
			 assertNotNull(cart.getDescription());
		}
	 
	 
	 @Test
		public void testFindprice() {
	    	Cart cart = new Cart();
			 cart.setPrice(100);
			 assertNotNull(cart.getPrice());
		}
	 
	 
	 @Test
		public void testFindquan() {
	    	Cart cart = new Cart();
			 cart.setQuantity(1);
			 assertNotNull(cart.getQuantity());
		}
	 
	 
	 
	 @Test
	 public void Constructor() {
	     
	     String name = "sony";
	     String description = "good";
	     String category = "headset";
	     float price = 200;
	     Integer quantity = 1;
	     Cart cart = new Cart(name, description, category, price, quantity);

	 }
}
