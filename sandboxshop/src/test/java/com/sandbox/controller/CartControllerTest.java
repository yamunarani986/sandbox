package com.sandbox.controller;

import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ui.Model;

import com.sandbox.controller.CartController;
import com.sandbox.model.Cart;
import com.sandbox.model.Category;
import com.sandbox.model.Orders;
import com.sandbox.service.CartService;
import com.sandbox.service.CategoryService;
import com.sandbox.service.OrderService;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class CartControllerTest {
	
	@InjectMocks
	CartController cartController;
	
	@Mock
	CartService service;
	
	@Mock
	OrderService oservice;
	
	@Mock
	private Model model;
	
	@Test
   	public void viewcartpage() {
		ArrayList<Cart> listcart = new ArrayList<Cart>();
		Cart cart = new Cart();
		cart.setId(6);
		cart.setCategory("Mobiles");
		cart.setDescription("Null");
		cart.setName("Sony");
		cart.setPrice(200);
		cart.setQuantity(1);
		listcart.add(cart);
		model.addAllAttributes(listcart);
		cartController.viewcartpage(model);
   	}
	
	@Test
   	public void deleteCart() {
		cartController.deleteCart(6);
   	}
	
	@Test
   	public void viewcart() {
		ArrayList<Orders> listorder = new ArrayList<Orders>();
		 Orders order = new Orders();
		 order.setId(90);
		 order.setName("yamuna");
		 order.setDescription("good");
		 order.setCategory("mobiles");
		 order.setPrice(200);
		 order.setQuantity(2);
		model.addAllAttributes(listorder);
		cartController.viewcart(model);
   	}
	
	@Test
   	public void saveSonyMobile() {
		ArrayList<Cart> listcart = new ArrayList<Cart>();
		Cart cart = new Cart();
		cart.setId(6);
		cart.setCategory("Mobiles");
		cart.setDescription("Null");
		cart.setName("Sony");
		cart.setPrice(200);
		cart.setQuantity(1);
		listcart.add(cart);
		model.addAllAttributes(listcart);
		cartController.saveSonyMobile(model);
   	}
	
	@Test
   	public void saveSonyreal() {
		ArrayList<Cart> listcart = new ArrayList<Cart>();
		Cart cart = new Cart();
		cart.setId(6);
		cart.setCategory("Mobiles");
		cart.setDescription("Null");
		cart.setName("Sony");
		cart.setPrice(200);
		cart.setQuantity(1);
		listcart.add(cart);
		model.addAllAttributes(listcart);
		cartController.saveSonyreal(model);
   	}
	
	@Test
   	public void saveOrder() {
		ArrayList<Cart> listcart = new ArrayList<Cart>();
		Cart cart = new Cart();
		cart.setId(6);
		cart.setCategory("Mobiles");
		cart.setDescription("Null");
		cart.setName("Sony");
		cart.setPrice(200);
		cart.setQuantity(1);
		listcart.add(cart);
		model.addAllAttributes(listcart);
		cartController.saveOrder(model);
   	}
   	}

