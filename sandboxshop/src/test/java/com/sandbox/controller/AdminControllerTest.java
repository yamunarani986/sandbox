package com.sandbox.controller;

import java.util.ArrayList;
import java.util.HashMap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ui.Model;

import com.sandbox.controller.AdminController;
import com.sandbox.model.Category;
import com.sandbox.model.Product;
import com.sandbox.service.CategoryService;
import com.sandbox.service.ProductService;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class AdminControllerTest {
	
	@InjectMocks
	AdminController adminController;
	
	@Mock
	CategoryService service;
	
	@Mock
	ProductService productservice;
	
	@Mock
	private Model model;
	
	@Test
   	public void viewadminHome() {
		adminController.viewadminHome();
   	}
	
	@Test
   	public void viewHomePage() {
		ArrayList<Category> listcategory = new ArrayList<Category>();
		Category category = new Category();
		category.setId((long) 6);
		category.setName("name");
		listcategory.add(category);
		model.addAllAttributes(listcategory);
		adminController.viewHomePage(model);
   	}
	
	@Test
   	public void newCategoryForm() {
		HashMap<String, Object> model1 = new HashMap<String, Object>();
		Category category = new Category();
		category.setId((long) 6);
		category.setName("name");
		model1.putAll(model1);
		adminController.newCategoryForm(model1);
   	}
	
	@Test
   	public void saveCategory() {
		Category category = new Category();
		category.setId((long) 6);
		category.setName("name");
		adminController.saveCategory(category);
   	}
	
	@Test
   	public void showEditcategoryPage() {
		adminController.showEditcategoryPage((long) 6);
   	}

	@Test
   	public void viewHome() {
		adminController.viewHome();
   	}
	
	@Test
   	public void deleteCategory() {
		adminController.deleteCategory((long) 6);
   	}
	
	@Test
   	public void viewproductspage() {
		ArrayList<Product> listproduct = new ArrayList<Product>();
		 Product product = new Product();
		 product.setPid((long) 6);
		 product.setName("name");
		 product.setCategory("category");
		 product.setDescription("Des");
		 product.setPrice(20);
		 product.setStock(1);
		listproduct.add(product);
		model.addAllAttributes(listproduct);
		adminController.viewproductspage(model);
   	}
	
	@Test
   	public void showNewProductPage() {
		Product product = new Product();
		 product.setPid((long) 6);
		 product.setName("name");
		 product.setCategory("category");
		 product.setDescription("Des");
		 product.setPrice(20);
		 product.setStock(1);
		 ArrayList<Category> listcategory = new ArrayList<Category>();
		Category category = new Category();
		category.setId((long) 6);
		category.setName("name");
		model.addAllAttributes(listcategory);
		adminController.showNewProductPage(model);
   	}
	
	@Test
   	public void saveProduct() {
		Product product = new Product();
		 product.setPid((long) 6);
		 product.setName("name");
		 product.setCategory("category");
		 product.setDescription("Des");
		 product.setPrice(20);
		 product.setStock(1);
		adminController.saveProduct(product);
   	}
	
	@Test
   	public void showEditProductPage() {
		 ArrayList<Category> listcategory = new ArrayList<Category>();
			Category category = new Category();
			category.setId((long) 6);
			category.setName("name");
			model.addAllAttributes(listcategory);
			adminController.showEditProductPage((long) 6, model);
   	}
	
	@Test
   	public void deleteProduct() {
		adminController.deleteProduct((long) 6);
   	}
	
	
}
