package com.sandbox.controller;

import javax.servlet.http.HttpSession;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.ui.Model;

import com.sandbox.controller.UserController;
import com.sandbox.model.UserDtls;
import com.sandbox.service.UserService;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class UserControllerTest {
	
	@InjectMocks
	UserController userController;
	
	@Mock
	private UserService userservice;
	
	@Mock
	private Model model;
	
	@Mock
	private HttpSession session;
	
	@Test
   	public void index() {
		userController.index();
   	}
	
	@Test
   	public void home() {
		userController.home();
   	}
	
	@Test
   	public void viewlogin() {
		userController.viewlogin();
   	}
	
	@Test
   	public void register() {
		userController.register();
   	}
	

	@Test
   	public void creatuser() {
		UserDtls user = new UserDtls();
	 	user.setId((long) 90);
        user.setFirstname("yamuna");
        user.setLastname("rani");
        user.setEmail("yamuna@gmail.com");
        user.setAddress("Hyd");
        user.setNumber("1234567890");
        user.setPassword("yamuna12");
        user.setRole("admin");
		userController.creatuser(user, session);
   	}
	
	@Test
   	public void creatnulluser() {
		UserDtls user = new UserDtls();
		session.setAttribute(null, user);
		userController.creatuser(user, session);
   	}
	
	@Test
   	public void about() {
		userController.about();
   	}
	
	@Test
   	public void viewlist() {
		userController.viewlist();
   	}
	
	@Test
   	public void vieworder() {
		userController.vieworder();
   	}
}
