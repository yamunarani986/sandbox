package com.sandbox.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.sandbox.model.Cart;
import com.sandbox.repository.CartRepository;



@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class CartServiceTests {
	

	@Mock
    private CartRepository repo;
    
    @InjectMocks
    private CartService service;
    
    @Test
   	public void savecart() {
       	Cart cart = new Cart();
       	cart.setId(1);
       	cart.setName("name");
       	cart.setCategory("Mobiles");
       	cart.setDescription("good");
       	cart.setPrice(200);
       	cart.setQuantity(1);
   		 service.save(cart);
   	}
    
    @Test
   	public void deletecart() {
   		 service.delete(1);
   	}
    
    @Test
   	public void listAll() {
		Cart cart = new Cart();
		cart.setId(1);
       	cart.setName("name");
       	cart.setCategory("Mobiles");
       	cart.setDescription("good");
       	cart.setPrice(200);
       	cart.setQuantity(1);
   		 service.listAll();
   	}
    
    @Test
   	public void totalPrice() {
    	service.totalPrice();
   	}
}
    
    

