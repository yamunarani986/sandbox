package com.sandbox.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.sandbox.model.Cart;
import com.sandbox.model.Orders;
import com.sandbox.repository.CartRepository;
import com.sandbox.repository.OrderRepository;



@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class OrderServiceTests {
	

	@Mock
    private OrderRepository repo;
    
    @InjectMocks
    private OrderService service;
    
    @Test
   	public void savecart() {
    	 Orders order = new Orders();
    	 order.setId(1);
    	 order.setName("name");
    	 order.setCategory("Mobiles");
    	 order.setDescription("good");
    	 order.setPrice(200);
    	 order.setQuantity(1);
   		 service.save(order);
   	}
    
    
   
    
    @Test
   	public void listAll() {
    	 Orders order = new Orders();
    	 order.setId(1);
    	 order.setName("name");
    	 order.setCategory("Mobiles");
    	 order.setDescription("good");
    	 order.setPrice(200);
    	 order.setQuantity(1);
   		 service.listAll();
   	}
    
    @Test
   	public void totalPrice() {
    	service.totalPrice();
   	}
    
}
    
    

