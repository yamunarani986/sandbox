package com.sandbox.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.sandbox.model.UserDtls;
import com.sandbox.repository.UserRepository;

@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class UserServiceTest {
	

	@Mock
    private UserRepository repo;
	
	@Mock
    private UserDtls user;
	
	@Mock
	private BCryptPasswordEncoder passwordEncode;
    
    @InjectMocks
    private UserServiceImpl service;
    
    @Test
   	public void createUser() {
    	 UserDtls user = new UserDtls();
         user.setPassword("$2a$10$GCvjL8yzA7C70EXjXc12Vu5Mmf2xMxa2cdAxE2Q9SkpghfMIwhLBu");
         passwordEncode.upgradeEncoding("yamuna");
         user.setRole("admin");
    	service.createUser(user);
    }
    
    @Test
   	public void checkEmail() {
   		 service.checkEmail(null);
   	}
    }
    
