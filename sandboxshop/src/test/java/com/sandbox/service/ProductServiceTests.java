package com.sandbox.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.sandbox.model.Product;
import com.sandbox.repository.ProductRepository;


@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class ProductServiceTests {
	

	@Mock
    private ProductRepository repo;
	
	@Mock
    private Product product;
    
    @InjectMocks
    private ProductService service;
    
    @Test
   	public void saveproduct() {
       	Product product = new Product();
       	product.setPid((long) 1);
   		product.setName("name");
   		product.setCategory("Mobiles");
   		product.setDescription("good");
   		product.setPrice(200);
   		product.setStock(1);
   		 service.save(product);
   	}
    
    @Test
   	public void deleteproduct() {
   		 service.delete((long) 1);
   	}
    
    @Test
   	public void listAll() {
    Product product = new Product();
	 product.setPid((long) 6);
	 product.setName("name");
	 product.setCategory("category");
	 product.setDescription("Des");
	 product.setPrice(20);
	 product.setStock(1);
	 service.listAll();
    }
}
    
    
