package com.sandbox.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.sandbox.model.Category;
import com.sandbox.repository.CategoryRepository;


@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
class CategoryServiceTests {
	

	@Mock
    private CategoryRepository repo;
    
    @InjectMocks
    private CategoryService service;
    
    @Test
   	public void savecategory() {
       	Category category = new Category();
       	category.setId((long) 1);
   		 category.setName("name");
   		 service.save(category);
   	}
    
    @Test
   	public void deletecategory() {
   		 service.delete((long) 1);
   	}
    
    @Test
   	public void listAll() {
	Category category = new Category();
	category.setId((long) 6);
	category.setName("name");
	service.listAll();
    }
    
}
    
    
