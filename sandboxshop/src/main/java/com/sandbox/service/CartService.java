package com.sandbox.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sandbox.model.Cart;
import com.sandbox.repository.CartRepository;

@Service
public class CartService {

	@Autowired
    private CartRepository repo;
     
    public List<Cart> listAll() {
		return repo.findAll();
	}
     
    public void save(Cart cart) {
        repo.save(cart);
    }
    public Cart get(Integer id) {
        return repo.findById(id).get();
    }
     
    public void delete(Integer id) {
        repo.deleteById(id);
    }
    
   
    public float totalPrice()
    {
    	float total=0;
   	List<Cart> crr =this.listAll();
    	for(Cart cart:crr)
    	{
   		total+=cart.getPrice();
    		}
   	return total;
    }

}
