package com.sandbox.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sandbox.model.Orders;

import com.sandbox.repository.OrderRepository;

@Service
public class OrderService {

	@Autowired
    private OrderRepository repo;
     
    public List<Orders> listAll() {
		return repo.findAll();
	}
     
    public void save(Orders order) {
        repo.save(order);
    }
    public Orders get(Integer id) {
        return repo.findById(id).get();
    }
     
    public float totalPrice()
    {
    	float total=0;
   	List<Orders> crr =this.listAll();
    	for(Orders cart:crr)
    	{
   		total+=cart.getPrice();
    		}
   	return total;
    }

}
