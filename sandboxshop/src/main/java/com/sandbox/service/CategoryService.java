package com.sandbox.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sandbox.model.Category;
import com.sandbox.repository.CategoryRepository;


@Service
public class CategoryService {

	@Autowired
    private CategoryRepository repo;
     
    public List<Category> listAll() {
		return repo.findAll();
	}
     
    public void save(Category category) {
        repo.save(category);
    }
     
    public Category get(Long id) {
        return repo.findById(id).get();
    }
     
    public void delete(Long id) {
        repo.deleteById(id);
    }
}
