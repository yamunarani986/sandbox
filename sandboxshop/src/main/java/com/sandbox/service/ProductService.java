package com.sandbox.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sandbox.model.Product;
import com.sandbox.repository.ProductRepository;

@Service
public class ProductService {

	@Autowired
    private ProductRepository repo;
     
    public List<Product> listAll() {
		return repo.findAll();
	}
     
    public void save(Product product) {
        repo.save(product);
    }
     
    public Product get(long pid) {
        return repo.findById(pid).get();
    }
     
    public void delete(long pid) {
        repo.deleteById(pid);
    }
}
