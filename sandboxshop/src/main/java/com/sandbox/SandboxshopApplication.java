package com.sandbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SandboxshopApplication {

	public static void main(String[] args) {
		SpringApplication.run(SandboxshopApplication.class, args);
	}

}
