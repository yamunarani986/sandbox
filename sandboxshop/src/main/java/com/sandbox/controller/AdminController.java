package com.sandbox.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.sandbox.model.Category;
import com.sandbox.model.Product;
import com.sandbox.service.CategoryService;
import com.sandbox.service.ProductService;


@Controller
public class AdminController {

	@Autowired
	CategoryService service;
	
	@Autowired
	ProductService productservice;
	
	@GetMapping("/Admin")
	public String viewadminHome() {
		return "adminHome";
	}	
		@GetMapping("/index")
		public String viewHome() {
			return "productlist";
}
		
	@GetMapping("/category")
	public String viewHomePage(Model model) {
		List<Category> listcategory = service.listAll();
	    model.addAttribute("listCategory", listcategory);
	     
	     
	    return "category";
	}
	
	@RequestMapping("/newcategory")
	public String newCategoryForm(Map<String, Object> model) {
		Category category = new Category();
	    model.put("category", category);
	    return "new_category";
	}
	
	
	@RequestMapping(value = "/savecategory", method = RequestMethod.POST)
	 public String saveCategory(@ModelAttribute("category") Category category) {
	     service.save(category);
	      
	     return "redirect:/category";
	 }	
	
	
	@RequestMapping("/editcategory/{id}")
	public ModelAndView showEditcategoryPage(@PathVariable(name = "id") Long id) {
	    ModelAndView mav = new ModelAndView("edit_category");
	    Category category = service.get(id);
	    mav.addObject("category", category);
	     
	    return mav;
	}
	
	@RequestMapping("/deletecategory/{id}")
	public String deleteCategory(@PathVariable(name = "id") Long id) {
	    service.delete(id);
	    return "redirect:/category";       
	}
	
	@GetMapping("/product")
	public String viewproductspage(Model model) {
		List<Product> listProducts = productservice.listAll();
	    model.addAttribute("listProducts", listProducts);
		return "products";
	}
	
	@RequestMapping("/newproduct")
	public String showNewProductPage(Model model) {
	    Product product = new Product();
	    model.addAttribute("product", product);
	    
	    List<Category> brand = service.listAll(); 
	    model.addAttribute("brand", brand);
	     
	    return "new_product";
	}
	
	
	@RequestMapping(value = "/saveproduct", method = RequestMethod.POST)
	public String saveProduct(@ModelAttribute("product") Product product) {
	    productservice.save(product);
	     
	    return "redirect:/product";
	}
	
	
	@RequestMapping("/editproduct/{pid}")
	public ModelAndView showEditProductPage(@PathVariable(name = "pid") Long pid, Model model) {
	    ModelAndView mav = new ModelAndView("edit_product");
	    Product product = productservice.get(pid);
	    mav.addObject("product", product);
	    
	    List<Category> brand = service.listAll(); 
	    model.addAttribute("brand", brand);
	     
	     
	    return mav;
	}
	
	@RequestMapping("/deleteproduct/{pid}")
	public String deleteProduct(@PathVariable(name = "pid") Long pid) {
	    productservice.delete(pid);
	    return "redirect:/product";       
	}
	
	
	
}
