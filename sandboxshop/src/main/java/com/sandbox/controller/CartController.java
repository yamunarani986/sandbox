package com.sandbox.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.sandbox.model.Cart;
import com.sandbox.model.Orders;
import com.sandbox.service.CartService;
import com.sandbox.service.OrderService;

@Controller
public class CartController {

	@Autowired
	private CartService service;
	
	@Autowired
	private OrderService oservice;
//	public float subtotal;
	
	@GetMapping("/cart")
	public String viewcartpage(Model model) {
		List<Cart> listCarts = service.listAll();
	    model.addAttribute("listCarts", listCarts);
		//subtotal =service.totalPrice();
		model.addAttribute("subtotal",service.totalPrice());
		return "cart";
	}
	

	
	@GetMapping(value = "/cartsony")
	 public String saveSonyMobile(Model model) {
		Cart cart = new Cart("sony with 5.4", "Sony A20, Reddish black", "Mobiles", 2200, 1);
		model.addAttribute("cart", cart);
	     service.save(cart);
	      
	     return "redirect:/listpro";
	 }	
	
	@GetMapping(value = "/cartsony1")
	 public String saveSonyreal(Model model) {
		Cart cart = new Cart("Real with 5.5 inch", "Real Me, Peach Green", "Mobiles", 3500, 1);
		model.addAttribute("cart", cart);
	     service.save(cart);
	      
	     return "redirect:/listpro";
	 }	

	@RequestMapping("/deletecart/{id}")
	public String deleteCart(@PathVariable(name = "id") Integer id) {
	    service.delete(id);
	    return "redirect:/cart";       
	}
	

	@GetMapping("/cart1")
	public String viewcart(Model model) {
		List<Orders> listOrders = oservice.listAll();
	    model.addAttribute("listOrders", listOrders);
	   return "cart1";
	}	
	@GetMapping(value = "/saveorder")
	public String saveOrder(Model model) {
		List<Cart> cart =service.listAll();
		for(Cart ct:cart)
		{
			Orders ord = new Orders(ct.getId(),ct.getName(),ct.getDescription(),ct.getCategory(),ct.getPrice(),ct.getQuantity());
			  oservice.save(ord);
		service.delete(ct.getId());	  
			}
		
		List<Orders> listOrders = oservice.listAll();
	    model.addAttribute("listOrders", listOrders);
	    model.addAttribute("subtotal",oservice.totalPrice());
	    return "cart1";
	}
	

}
