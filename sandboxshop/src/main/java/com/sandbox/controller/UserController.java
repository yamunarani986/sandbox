package com.sandbox.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sandbox.model.Category;
import com.sandbox.model.UserDtls;
import com.sandbox.service.UserService;

@Controller
public class UserController {

	@Autowired
	private UserService userservice;
	
	@GetMapping("/")
	public String index() {
		return "home1";
}
	
	@GetMapping("/home")
	public String home() {
		return "home";
}
	@GetMapping("/signin")
	public String viewlogin() {
		return "login";
	}	
		@GetMapping("/register")
		public String register() {
			return "register";
}
		
		@PostMapping("/createUser")
		public String creatuser(@ModelAttribute UserDtls user,HttpSession session) {
			
			boolean f=userservice.checkEmail(user.getEmail());
			
			if(f)
		{
				session.setAttribute("msg", "Email Id already Exists");
		}else {
			UserDtls userDtls = userservice.createUser(user);
			if(userDtls !=null) {
				session.setAttribute("msg", "Register Sucessfully");
			}else {
				session.setAttribute("msg", "something error in server");
			}
		}
			return "redirect:/register";
			}
		
		@GetMapping("/about")
		public String about() {
			return "about";
		}	


		@GetMapping("/listpro")
		public String viewlist() {
			return "mobiles";
		}
		

		@GetMapping("/ordersuccess")
		public String vieworder() {
			return "ordersuccess";
		}
		
		
		
			
}
	
