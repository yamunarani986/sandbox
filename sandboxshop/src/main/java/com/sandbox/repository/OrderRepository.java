package com.sandbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sandbox.model.Orders;

public interface OrderRepository extends JpaRepository<Orders, Integer> {

}
