package com.sandbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sandbox.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

	
}
