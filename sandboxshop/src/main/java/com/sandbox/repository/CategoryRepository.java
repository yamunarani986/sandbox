package com.sandbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sandbox.model.Category;


public interface CategoryRepository extends JpaRepository<Category, Long> {

	Object findByName(String name);

	Object findById(String name);

}

