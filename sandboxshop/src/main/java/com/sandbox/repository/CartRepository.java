package com.sandbox.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sandbox.model.Cart;

public interface CartRepository extends JpaRepository<Cart, Integer> {

}
