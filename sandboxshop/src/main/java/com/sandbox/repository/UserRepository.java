package com.sandbox.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.sandbox.model.UserDtls;


public interface UserRepository extends JpaRepository<UserDtls, Long> {

	public boolean existsByEmail(String email);
	
	@Query("SELECT u FROM UserDtls u WHERE u.email = ?1")
	public UserDtls findByEmail(String email);

	
}
